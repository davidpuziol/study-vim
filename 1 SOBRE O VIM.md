# About

O vim é um dos editores mais antigos de todos e extremamente personalizável.

O modo com que o `vim` foi pensado reflete a histório dos computadores. Foi criado em uma época que não existia mouse e o teclado era diferente com menos teclas e posições diferentes. Não existia page down, page up, setas, teclado númerico, etc.

![teclado](./pics/tecladoadm3a.jpg)

>Observer a posição das setas em `H J K L` o `esc`por exemplo.

Monitores eram impressoras por isso não dava para ficar imprimindo todo o código. Não era possível carregar todo o código nos computadores com pouca memória, por isso era necessário um editor direto ao ponto. Por isso o Vim é tão poderoso, ele é DIRETO AO PONTO.

## Aprendizado

Aprender o Vim requer tempo e dedicação. A curva de aprendizado é longa. Se cada vez que a gente ler sobre o vim conseguir fixar um ou dois atalhos e aplicá-los no nosso dia a dia vai chegar uma hora que os atalhos se somam e a memória muscular vai fazer com que tudo se torne automático. Não adianta achar que vai sair lembrando tudo pois é humanamente impossível.

> É possível encontrar alguns adesivos e keycaps para teclados com atalhos do vim para te ajudar a lembrar durante um tempo

![atalhos](./pics/vimkeyboard.png)

## Modo do vim

O vim tem 3 modos principais. O modo normal que é o modo de navegação, o modo de comando que e o modo de inserção.

>Também existem outros modos porém pouco utilizados.

O Vim deve ser utilizado em seu modo normal e somente entrar no modo de inserção no momento que for necessário inserir. Se não for para inserir um caractere e somente manipular os que já existem como por exemplo recortar, colar, deletar, etc, é poissível fazer tudo pelo modo normal. O modo normal é considerado o meio e dele vc vai para qualquer modo, porém de um outro modo somente volta para o modo normal

Cada modo pode mudar os atalhos do teclado, por isso que a curva se torna longa e é necessário muita prática.

A tecla que te tira de qualquer comando é o `ESC`

>Uma dica para lembrar os atalhos é que geralmente um atalho faz algo e o mesmo atalho com shift faz algo muito similar, geralmente antagônico.
